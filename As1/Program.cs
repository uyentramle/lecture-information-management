﻿using As1;
using System.Linq.Expressions;

class Program
{
	static void Main(string[] args)
	{
		// 1. Tạo một List hoặc Array Giáo Viên với số lượng phần tử nhập từ bàn phím
		List<GiaoVien> dsGiaoVien = new List<GiaoVien>();
		Console.Write("Nhap so luong giao vien: ");
		int soLuong = int.Parse(Console.ReadLine());
		//if (!int.TryParse(Console.ReadLine(), out int soLuong) || soLuong < 0)
		//{
		//	Console.WriteLine("So luong khong hop le");
		//	return;
		//}

		// 2. Nhập các thông tin cho từng phần tử với thông tin từ bàn phím
		for (int i = 1; i <= soLuong; i++)
		{
			Console.WriteLine($"NHAP THONG TIN GIAO VIEN THU {i}: ");
			Console.Write("Ho ten: ");
			string hoTen = Console.ReadLine();

			Console.Write("Nam sinh: ");
			//int namSinh = int.Parse(Console.ReadLine());
			int namSinh = NhapNamSinh();

			Console.Write("Luong co ban: ");
			double luongCoBan = double.Parse(Console.ReadLine());
			//if (!double.TryParse(Console.ReadLine(), out double luongCoBan) || luongCoBan < 0)
			//{
			//	Console.WriteLine("Luong Co ban khong hop le, vui long nhap lai!");
			//	i--; continue;
			//}

			Console.Write("He so luong: ");
			double heSoLuong = double.Parse(Console.ReadLine());
			//if (!double.TryParse(Console.ReadLine(), out double heSoLuong) || heSoLuong < 0)
			//{
			//	Console.WriteLine("He so luong khong hop le, vui long nhap lai!");
			//	i--;
			//	continue;
			//}

			GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
			dsGiaoVien.Add(giaoVien);
		}

		// 3. Hiển thị thông tin của GiaoVien có lương thấp nhất
		GiaoVien giaoVienLuongThapNhat = dsGiaoVien.OrderBy(gv => gv.TinhLuong()).FirstOrDefault();
		if (giaoVienLuongThapNhat != null)
		{
			Console.WriteLine("Thong tin giao vien co luong thap nhat: ");
			giaoVienLuongThapNhat.XuatThongTin();
		}
		else
		{
			Console.WriteLine("Khong co giao vien nao trong danh sach");
		}

		/* 4. Dừng màn hình để xem kết quả 
		 * (Do trên ứng dụng Console sau khi chạy câu lệnh Console.WriteLine() sẽ in ra kết quả và tắt chương trình, 
		 * nên khi các bạn muốn dừng màn hình xem kết quả thì sử dụng câu lệnh Console.Read() 
		 * để chờ ấn một phím bất kỳ từ bàn phím, trong khi chờ các bạn có thể kiểm tra kết quả của mình) 
		 */
		Console.Read();

		// 5. Hiển thị thông báo lỗi ngoại lệ (Nếu có), tìm phương án sửa lỗi sau khi tìm được vị trí gây lỗi
	}

	static int NhapNamSinh()
	{
		int NamSinh;
		while (true)
		{
			Console.Write("Nam sinh (yyyy): ");
			string NamSinhStr = Console.ReadLine();
			if (!int.TryParse(NamSinhStr, out NamSinh) && NamSinhStr.Length == 4)
			{
				return NamSinh;
			} else
			{
				Console.WriteLine("Nam sinh khong hop le. Vui long nhap lai!");
			}
		}
	}
}